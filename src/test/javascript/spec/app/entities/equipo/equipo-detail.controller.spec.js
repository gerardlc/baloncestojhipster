'use strict';

describe('Equipo Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockEquipo, MockJugador, MockEntrenador, MockEstadio, MockSocio, MockPartido, MockTemporada;
    var createController;

    beforeEach(inject(function($injector) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        MockEntity = jasmine.createSpy('MockEntity');
        MockEquipo = jasmine.createSpy('MockEquipo');
        MockJugador = jasmine.createSpy('MockJugador');
        MockEntrenador = jasmine.createSpy('MockEntrenador');
        MockEstadio = jasmine.createSpy('MockEstadio');
        MockSocio = jasmine.createSpy('MockSocio');
        MockPartido = jasmine.createSpy('MockPartido');
        MockTemporada = jasmine.createSpy('MockTemporada');
        

        var locals = {
            '$scope': $scope,
            '$rootScope': $rootScope,
            'entity': MockEntity ,
            'Equipo': MockEquipo,
            'Jugador': MockJugador,
            'Entrenador': MockEntrenador,
            'Estadio': MockEstadio,
            'Socio': MockSocio,
            'Partido': MockPartido,
            'Temporada': MockTemporada
        };
        createController = function() {
            $injector.get('$controller')("EquipoDetailController", locals);
        };
    }));


    describe('Root Scope Listening', function() {
        it('Unregisters root scope listener upon scope destruction', function() {
            var eventType = 'baloncestoApp:equipoUpdate';

            createController();
            expect($rootScope.$$listenerCount[eventType]).toEqual(1);

            $scope.$destroy();
            expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
        });
    });
});
