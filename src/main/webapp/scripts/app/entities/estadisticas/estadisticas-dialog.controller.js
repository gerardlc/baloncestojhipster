'use strict';

angular.module('baloncestoApp').controller('EstadisticasDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Estadisticas', 'Jugador', 'Partido',
        function($scope, $stateParams, $uibModalInstance, entity, Estadisticas, Jugador, Partido) {

        $scope.estadisticas = entity;
        $scope.jugadors = Jugador.query();
        $scope.partidos = Partido.query();
        $scope.load = function(id) {
            Estadisticas.get({id : id}, function(result) {
                $scope.estadisticas = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('baloncestoApp:estadisticasUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.estadisticas.id != null) {
                Estadisticas.update($scope.estadisticas, onSaveSuccess, onSaveError);
            } else {
                Estadisticas.save($scope.estadisticas, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
