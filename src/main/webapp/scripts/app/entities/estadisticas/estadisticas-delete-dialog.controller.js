'use strict';

angular.module('baloncestoApp')
	.controller('EstadisticasDeleteController', function($scope, $uibModalInstance, entity, Estadisticas) {

        $scope.estadisticas = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Estadisticas.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
