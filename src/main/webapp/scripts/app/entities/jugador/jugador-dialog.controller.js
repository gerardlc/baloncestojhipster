'use strict';

angular.module('baloncestoApp').controller('JugadorDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Jugador', 'Equipo', 'Estadisticas',
        function($scope, $stateParams, $uibModalInstance, $q, entity, Jugador, Equipo, Estadisticas) {

        $scope.jugador = entity;
        $scope.equipos = Equipo.query({filter: 'jugador-is-null'});
        $q.all([$scope.jugador.$promise, $scope.equipos.$promise]).then(function() {
            if (!$scope.jugador.equipo || !$scope.jugador.equipo.id) {
                return $q.reject();
            }
            return Equipo.get({id : $scope.jugador.equipo.id}).$promise;
        }).then(function(equipo) {
            $scope.equipos.push(equipo);
        });
        $scope.equipos = Equipo.query();
        $scope.estadisticass = Estadisticas.query();
        $scope.load = function(id) {
            Jugador.get({id : id}, function(result) {
                $scope.jugador = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('baloncestoApp:jugadorUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.jugador.id != null) {
                Jugador.update($scope.jugador, onSaveSuccess, onSaveError);
            } else {
                Jugador.save($scope.jugador, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
