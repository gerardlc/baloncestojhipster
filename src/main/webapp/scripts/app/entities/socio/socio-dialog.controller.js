'use strict';

angular.module('baloncestoApp').controller('SocioDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'Socio', 'Equipo',
        function($scope, $stateParams, $uibModalInstance, DataUtils, entity, Socio, Equipo) {

        $scope.socio = entity;
        $scope.equipos = Equipo.query();
        $scope.load = function(id) {
            Socio.get({id : id}, function(result) {
                $scope.socio = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('baloncestoApp:socioUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.socio.id != null) {
                Socio.update($scope.socio, onSaveSuccess, onSaveError);
            } else {
                Socio.save($scope.socio, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.abbreviate = DataUtils.abbreviate;

        $scope.byteSize = DataUtils.byteSize;

        $scope.setFoto = function ($file, socio) {
            if ($file && $file.$error == 'pattern') {
                return;
            }
            if ($file) {
                var fileReader = new FileReader();
                fileReader.readAsDataURL($file);
                fileReader.onload = function (e) {
                    var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                    $scope.$apply(function() {
                        socio.foto = base64Data;
                        socio.fotoContentType = $file.type;
                    });
                };
            }
        };
}]);
