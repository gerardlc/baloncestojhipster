'use strict';

angular.module('baloncestoApp')
	.controller('SocioDeleteController', function($scope, $uibModalInstance, entity, Socio) {

        $scope.socio = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Socio.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
