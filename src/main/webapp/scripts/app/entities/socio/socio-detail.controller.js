'use strict';

angular.module('baloncestoApp')
    .controller('SocioDetailController', function ($scope, $rootScope, $stateParams, DataUtils, entity, Socio, Equipo) {
        $scope.socio = entity;
        $scope.load = function (id) {
            Socio.get({id: id}, function(result) {
                $scope.socio = result;
            });
        };
        var unsubscribe = $rootScope.$on('baloncestoApp:socioUpdate', function(event, result) {
            $scope.socio = result;
        });
        $scope.$on('$destroy', unsubscribe);

        $scope.byteSize = DataUtils.byteSize;
    });
