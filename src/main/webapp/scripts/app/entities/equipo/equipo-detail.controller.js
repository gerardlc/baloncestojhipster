'use strict';

angular.module('baloncestoApp')
    .controller('EquipoDetailController', function ($scope, $rootScope, $stateParams, entity,maxCanastasJugador, Equipo, Jugador, Entrenador, Estadio, Socio, Partido, Temporada) {
        $scope.equipo = entity;
        $scope.maxCanastasJugador = maxCanastasJugador;
        $scope.load = function (id) {
            Equipo.get({id: id}, function(result) {
                $scope.equipo = result;
            });
            Equipo.maxCanastasJugador({id: id}, function(result) {
                $scope.maxCanastasJugador = result;
            });
        };
        var unsubscribe = $rootScope.$on('baloncestoApp:equipoUpdate', function(event, result) {
            $scope.equipo = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
