'use strict';

angular.module('baloncestoApp')
	.controller('LigaDeleteController', function($scope, $uibModalInstance, entity, Liga) {

        $scope.liga = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Liga.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
