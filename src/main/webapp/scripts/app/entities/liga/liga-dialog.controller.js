'use strict';

angular.module('baloncestoApp').controller('LigaDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Liga', 'Temporada', 'Arbitro',
        function($scope, $stateParams, $uibModalInstance, entity, Liga, Temporada, Arbitro) {

        $scope.liga = entity;
        $scope.temporadas = Temporada.query();
        $scope.arbitros = Arbitro.query();
        $scope.load = function(id) {
            Liga.get({id : id}, function(result) {
                $scope.liga = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('baloncestoApp:ligaUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.liga.id != null) {
                Liga.update($scope.liga, onSaveSuccess, onSaveError);
            } else {
                Liga.save($scope.liga, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
