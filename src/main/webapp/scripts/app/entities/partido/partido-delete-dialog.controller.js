'use strict';

angular.module('baloncestoApp')
	.controller('PartidoDeleteController', function($scope, $uibModalInstance, entity, Partido) {

        $scope.partido = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Partido.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
