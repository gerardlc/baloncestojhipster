'use strict';

angular.module('baloncestoApp').controller('PartidoDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Partido', 'Estadisticas', 'Equipo', 'Temporada', 'Arbitro',
        function($scope, $stateParams, $uibModalInstance, entity, Partido, Estadisticas, Equipo, Temporada, Arbitro) {

        $scope.partido = entity;
        $scope.estadisticass = Estadisticas.query();
        $scope.equipos = Equipo.query();
        $scope.temporadas = Temporada.query();
        $scope.arbitros = Arbitro.query();
        $scope.load = function(id) {
            Partido.get({id : id}, function(result) {
                $scope.partido = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('baloncestoApp:partidoUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.partido.id != null) {
                Partido.update($scope.partido, onSaveSuccess, onSaveError);
            } else {
                Partido.save($scope.partido, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
