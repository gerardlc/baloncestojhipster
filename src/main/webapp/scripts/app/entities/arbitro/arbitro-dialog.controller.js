'use strict';

angular.module('baloncestoApp').controller('ArbitroDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Arbitro', 'Liga', 'Partido',
        function($scope, $stateParams, $uibModalInstance, entity, Arbitro, Liga, Partido) {

        $scope.arbitro = entity;
        $scope.ligas = Liga.query();
        $scope.partidos = Partido.query();
        $scope.load = function(id) {
            Arbitro.get({id : id}, function(result) {
                $scope.arbitro = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('baloncestoApp:arbitroUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.arbitro.id != null) {
                Arbitro.update($scope.arbitro, onSaveSuccess, onSaveError);
            } else {
                Arbitro.save($scope.arbitro, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
