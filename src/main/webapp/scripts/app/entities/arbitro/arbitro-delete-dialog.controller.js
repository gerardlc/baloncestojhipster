'use strict';

angular.module('baloncestoApp')
	.controller('ArbitroDeleteController', function($scope, $uibModalInstance, entity, Arbitro) {

        $scope.arbitro = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Arbitro.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
