'use strict';

angular.module('baloncestoApp')
    .controller('ArbitroDetailController', function ($scope, $rootScope, $stateParams, entity, Arbitro, Liga, Partido) {
        $scope.arbitro = entity;
        $scope.load = function (id) {
            Arbitro.get({id: id}, function(result) {
                $scope.arbitro = result;
            });
        };
        var unsubscribe = $rootScope.$on('baloncestoApp:arbitroUpdate', function(event, result) {
            $scope.arbitro = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
