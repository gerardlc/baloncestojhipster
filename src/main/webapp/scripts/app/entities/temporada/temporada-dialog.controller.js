'use strict';

angular.module('baloncestoApp').controller('TemporadaDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Temporada', 'Liga', 'Equipo', 'Partido',
        function($scope, $stateParams, $uibModalInstance, entity, Temporada, Liga, Equipo, Partido) {

        $scope.temporada = entity;
        $scope.ligas = Liga.query();
        $scope.equipos = Equipo.query();
        $scope.partidos = Partido.query();
        $scope.load = function(id) {
            Temporada.get({id : id}, function(result) {
                $scope.temporada = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('baloncestoApp:temporadaUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.temporada.id != null) {
                Temporada.update($scope.temporada, onSaveSuccess, onSaveError);
            } else {
                Temporada.save($scope.temporada, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
