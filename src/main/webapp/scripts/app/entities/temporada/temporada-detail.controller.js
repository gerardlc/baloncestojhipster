'use strict';

angular.module('baloncestoApp')
    .controller('TemporadaDetailController', function ($scope, $rootScope, $stateParams, entity, Temporada, Liga, Equipo, Partido) {
        $scope.temporada = entity;
        $scope.ultimopartido=null;
        $scope.load = function (id) {
            Temporada.get({id: id}, function(result) {
                $scope.temporada = result;
            });

            /*
             * Ejercicio ultimo partido
             */
            Temporada.ultimopartido({id: '1'}, function(result) {
                $scope.ultimopartido = result;
            });
        };
        $scope.load();
        var unsubscribe = $rootScope.$on('baloncestoApp:temporadaUpdate', function(event, result) {
            $scope.temporada = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
