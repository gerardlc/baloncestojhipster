'use strict';

angular.module('baloncestoApp')
	.controller('TemporadaDeleteController', function($scope, $uibModalInstance, entity, Temporada) {

        $scope.temporada = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Temporada.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
