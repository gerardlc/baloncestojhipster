'use strict';

angular.module('baloncestoApp')
	.controller('EstadioDeleteController', function($scope, $uibModalInstance, entity, Estadio) {

        $scope.estadio = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Estadio.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
