'use strict';

angular.module('baloncestoApp').controller('EstadioDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Estadio', 'Equipo',
        function($scope, $stateParams, $uibModalInstance, entity, Estadio, Equipo) {

        $scope.estadio = entity;
        $scope.equipos = Equipo.query();
        $scope.load = function(id) {
            Estadio.get({id : id}, function(result) {
                $scope.estadio = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('baloncestoApp:estadioUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.estadio.id != null) {
                Estadio.update($scope.estadio, onSaveSuccess, onSaveError);
            } else {
                Estadio.save($scope.estadio, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
