'use strict';

angular.module('baloncestoApp')
    .factory('Equipo', function ($resource, DateUtils) {
        return $resource('api/equipos/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.fechaCracion = DateUtils.convertLocaleDateFromServer(data.fechaCracion);
                    return data;
                }
            },
            'maxCanastasJugador': {
                            method: 'GET',
                            transformResponse: function (data) {
                                data = angular.fromJson(data);
                                data.fechaCreacion = DateUtils.convertLocaleDateFromServer(data.fechaCreacion);
                                return data;
                            },url:'api/equipos/:id/maxCanastasJugador'
            },
            'jugadoresEquipo': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.fechaCreacion = DateUtils.convertLocaleDateFromServer(data.fechaCreacion);
                    return data;
                },url:'api/equipos/:id/jugadores'
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.fechaCracion = DateUtils.convertLocaleDateToServer(data.fechaCracion);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.fechaCracion = DateUtils.convertLocaleDateToServer(data.fechaCracion);
                    return angular.toJson(data);
                }
            }
        });
    });
