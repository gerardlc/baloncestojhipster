 'use strict';

angular.module('baloncestoApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-baloncestoApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-baloncestoApp-params')});
                }
                return response;
            }
        };
    });
