package com.gerardlc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.gerardlc.domain.Equipo;
import com.gerardlc.domain.Jugador;
import com.gerardlc.repository.EquipoRepository;
import com.gerardlc.web.rest.util.HeaderUtil;
import com.gerardlc.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing Equipo.
 */
@RestController
@RequestMapping("/api")
public class EquipoResource {

    private final Logger log = LoggerFactory.getLogger(EquipoResource.class);

    @Inject
    private EquipoRepository equipoRepository;

    /**
     * POST  /equipos -> Create a new equipo.
     */
    @RequestMapping(value = "/equipos",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Equipo> createEquipo(@RequestBody Equipo equipo) throws URISyntaxException {
        log.debug("REST request to save Equipo : {}", equipo);
        if (equipo.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("equipo", "idexists", "A new equipo cannot already have an ID")).body(null);
        }
        Equipo result = equipoRepository.save(equipo);
        return ResponseEntity.created(new URI("/api/equipos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("equipo", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /equipos -> Updates an existing equipo.
     */
    @RequestMapping(value = "/equipos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Equipo> updateEquipo(@RequestBody Equipo equipo) throws URISyntaxException {
        log.debug("REST request to update Equipo : {}", equipo);
        if (equipo.getId() == null) {
            return createEquipo(equipo);
        }
        Equipo result = equipoRepository.save(equipo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("equipo", equipo.getId().toString()))
            .body(result);
    }

    /**
     * GET  /equipos -> get all the equipos.
     */
    @RequestMapping(value = "/equipos",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Equipo>> getAllEquipos(Pageable pageable, @RequestParam(required = false) String filter)
        throws URISyntaxException {
        if ("jugador-is-null".equals(filter)) {
            log.debug("REST request to get all Equipos where jugador is null");
            return new ResponseEntity<>(StreamSupport
                .stream(equipoRepository.findAll().spliterator(), false)
                .filter(equipo -> equipo.getJugador() == null)
                .collect(Collectors.toList()), HttpStatus.OK);
        }
        log.debug("REST request to get a page of Equipos");
        Page<Equipo> page = equipoRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/equipos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /equipos/:id -> get the "id" equipo.
     */
    @RequestMapping(value = "/equipos/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Equipo> getEquipo(@PathVariable Long id) {
        log.debug("REST request to get Equipo : {}", id);
        Equipo equipo = equipoRepository.findOneWithEagerRelationships(id);
        return Optional.ofNullable(equipo)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /equipos/:id/maxCanastasJugador ->jugador con mas acanastas de un equipo
     */


        @RequestMapping(value = "/equipos/{id}/maxCanastasJugador",
                method = RequestMethod.GET,
                produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        public ResponseEntity<Jugador> maxCanastasJugador(@PathVariable Long id) {
                log.debug("REST request to get Equipo : {}", id);

                    Equipo equipo = equipoRepository.findOne(id);

                    if(equipo == null){
                        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                    }

            /* lista de jugadores del equipo ordenada por canastas  */
                    List<Jugador> jugadores = equipoRepository.findByEquipoOrderByCanastasTotal(id);

            //devuelve el jugador con mas canastas
                    return new ResponseEntity<>(
                        jugadores.get(0),
                        HttpStatus.OK);
            }

    /**
     * GET  /equipos/:id/jugadores ->jugadores de un equipo ex9 angular
     */
    @RequestMapping(value = "/equipos/{id}/jugadores",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Jugador>> getjugadoresEquipo(@PathVariable Long id)
        throws URISyntaxException {

        List<Jugador> jugadores = equipoRepository.findJugadoresEquipo(id);

        return new ResponseEntity<>(jugadores, HttpStatus.OK);
    }




    /**
     * DELETE  /equipos/:id -> delete the "id" equipo.
     */
    @RequestMapping(value = "/equipos/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteEquipo(@PathVariable Long id) {
        log.debug("REST request to delete Equipo : {}", id);
        equipoRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("equipo", id.toString())).build();
    }




}
