package com.gerardlc.repository;

import com.gerardlc.domain.Equipo;

import com.gerardlc.domain.Jugador;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Equipo entity.
 */
public interface EquipoRepository extends JpaRepository<Equipo,Long> {

    @Query("select distinct equipo from Equipo equipo left join fetch equipo.socioss left join fetch equipo.equipoTemporadas")
    List<Equipo> findAllWithEagerRelationships();

    @Query("select equipo from Equipo equipo left join fetch equipo.socioss left join fetch equipo.equipoTemporadas where equipo.id =:id")
    Equipo findOneWithEagerRelationships(@Param("id") Long id);

    @Query("SELECT j from Jugador j WHERE j.jugadorEquipo.id=:id order by j.canastasTotal desc")
    List<Jugador> findByEquipoOrderByCanastasTotal(@Param("id") Long id);

    @Query("SELECT j from Jugador j WHERE j.jugadorEquipo.id=:id ")
    List<Jugador> findJugadoresEquipo(@Param("id") Long id);

}
