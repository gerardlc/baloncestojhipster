package com.gerardlc.repository;

import com.gerardlc.domain.Jugador;

import org.springframework.data.repository.query.Param;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Jugador entity.
 */
public interface JugadorRepository extends JpaRepository<Jugador,Long> {


}
