package com.gerardlc.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import java.time.LocalDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Equipo.
 */
@Entity
@Table(name = "equipo")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Equipo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "localidad")
    private String localidad;

    @Column(name = "fecha_cracion")
    private LocalDate fechaCracion;

    @OneToOne(mappedBy = "equipo")
    @JsonIgnore
    private Jugador jugador;

    @OneToMany(mappedBy = "jugadorEquipo")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Jugador> jugadoress = new HashSet<>();

    @OneToOne    private Entrenador entrenador;

    @OneToOne    private Estadio estadio;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "equipo_socios",
               joinColumns = @JoinColumn(name="equipos_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="socioss_id", referencedColumnName="ID"))
    private Set<Socio> socioss = new HashSet<>();

    @OneToMany(mappedBy = "equipoLocal")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Partido> localEquipos = new HashSet<>();

    @OneToMany(mappedBy = "equipoVisitante")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Partido> visitanteEquipos = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "equipo_equipo_temporada",
               joinColumns = @JoinColumn(name="equipos_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="equipo_temporadas_id", referencedColumnName="ID"))
    private Set<Temporada> equipoTemporadas = new HashSet<>();

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public LocalDate getFechaCracion() {
        return fechaCracion;
    }

    public void setFechaCracion(LocalDate fechaCracion) {
        this.fechaCracion = fechaCracion;
    }

    public Jugador getJugador() {
        return jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    public Set<Jugador> getJugadoress() {
        return jugadoress;
    }

    public void setJugadoress(Set<Jugador> jugadors) {
        this.jugadoress = jugadors;
    }

    public Entrenador getEntrenador() {
        return entrenador;
    }

    public void setEntrenador(Entrenador entrenador) {
        this.entrenador = entrenador;
    }

    public Estadio getEstadio() {
        return estadio;
    }

    public void setEstadio(Estadio estadio) {
        this.estadio = estadio;
    }

    public Set<Socio> getSocioss() {
        return socioss;
    }

    public void setSocioss(Set<Socio> socios) {
        this.socioss = socios;
    }

    public Set<Partido> getLocalEquipos() {
        return localEquipos;
    }

    public void setLocalEquipos(Set<Partido> partidos) {
        this.localEquipos = partidos;
    }

    public Set<Partido> getVisitanteEquipos() {
        return visitanteEquipos;
    }

    public void setVisitanteEquipos(Set<Partido> partidos) {
        this.visitanteEquipos = partidos;
    }

    public Set<Temporada> getEquipoTemporadas() {
        return equipoTemporadas;
    }

    public void setEquipoTemporadas(Set<Temporada> temporadas) {
        this.equipoTemporadas = temporadas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Equipo equipo = (Equipo) o;
        return Objects.equals(id, equipo.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Equipo{" +
            "id=" + id +
            ", nombre='" + nombre + "'" +
            ", localidad='" + localidad + "'" +
            ", fechaCracion='" + fechaCracion + "'" +
            '}';
    }
}
