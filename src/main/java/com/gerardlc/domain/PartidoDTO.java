package com.gerardlc.domain;

import javax.persistence.Column;
import java.time.LocalDate;

/**
 * Created by gerard on 14/12/2015.
 */
public class PartidoDTO {

    private LocalDate fecha;

    private Integer puntosLocal;

    private Integer puntosVisitante;

    private String nombreEquipoLocal;

    private String nombreEquipoVisitante;

    private String nombreArbitro;

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public Integer getPuntosLocal() {
        return puntosLocal;
    }

    public void setPuntosLocal(Integer puntosLocal) {
        this.puntosLocal = puntosLocal;
    }

    public Integer getPuntosVisitante() {
        return puntosVisitante;
    }

    public void setPuntosVisitante(Integer puntosVisitante) {
        this.puntosVisitante = puntosVisitante;
    }

    public String getNombreEquipoLocal() {
        return nombreEquipoLocal;
    }

    public void setNombreEquipoLocal(String nombreEquipoLocal) {
        this.nombreEquipoLocal = nombreEquipoLocal;
    }

    public String getNombreEquipoVisitante() {
        return nombreEquipoVisitante;
    }

    public void setNombreEquipoVisitante(String nombreEquipoVisitante) {
        this.nombreEquipoVisitante = nombreEquipoVisitante;
    }

    public String getNombreArbitro() {
        return nombreArbitro;
    }

    public void setNombreArbitro(String nombreArbitro) {
        this.nombreArbitro = nombreArbitro;
    }

    @Override
    public String toString() {
        return "PartidoDTO{" +
            "fecha=" + fecha +
            ", puntosLocal=" + puntosLocal +
            ", puntosVisitante=" + puntosVisitante +
            ", nombreEquipoLocal='" + nombreEquipoLocal + '\'' +
            ", nombreEquipoVisitante='" + nombreEquipoVisitante + '\'' +
            ", nombreArbitro='" + nombreArbitro + '\'' +
            '}';
    }
}
