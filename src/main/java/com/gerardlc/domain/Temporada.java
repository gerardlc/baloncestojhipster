package com.gerardlc.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Temporada.
 */
@Entity
@Table(name = "temporada")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Temporada implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "anyo")
    private Integer anyo;

    @ManyToOne
    @JoinColumn(name = "liga_id")
    private Liga liga;

    @ManyToMany(mappedBy = "equipoTemporadas")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Equipo> temporadaEquiposs = new HashSet<>();

    @OneToMany(mappedBy = "temporada")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Partido> temporadaPartidoss = new HashSet<>();

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAnyo() {
        return anyo;
    }

    public void setAnyo(Integer anyo) {
        this.anyo = anyo;
    }

    public Liga getLiga() {
        return liga;
    }

    public void setLiga(Liga liga) {
        this.liga = liga;
    }

    public Set<Equipo> getTemporadaEquiposs() {
        return temporadaEquiposs;
    }

    public void setTemporadaEquiposs(Set<Equipo> equipos) {
        this.temporadaEquiposs = equipos;
    }

    public Set<Partido> getTemporadaPartidoss() {
        return temporadaPartidoss;
    }

    public void setTemporadaPartidoss(Set<Partido> partidos) {
        this.temporadaPartidoss = partidos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Temporada temporada = (Temporada) o;
        return Objects.equals(id, temporada.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Temporada{" +
            "id=" + id +
            ", anyo='" + anyo + "'" +
            '}';
    }
}
