package com.gerardlc.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import java.time.LocalDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Jugador.
 */
@Entity
@Table(name = "jugador")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Jugador implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "fecha_nacimiento")
    private LocalDate fechaNacimiento;

    @Column(name = "canastas_total")
    private Integer canastasTotal;

    @Column(name = "asistencias_total")
    private Integer asistenciasTotal;

    @Column(name = "rebotes_total")
    private Integer rebotesTotal;

    @Column(name = "posicion")
    private String posicion;

    @OneToOne    private Equipo equipo;

    @ManyToOne
    @JoinColumn(name = "jugador_equipo_id")
    private Equipo jugadorEquipo;

    @OneToMany(mappedBy = "jugador")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Estadisticas> jugadorEstadisticass = new HashSet<>();

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Integer getCanastasTotal() {
        return canastasTotal;
    }

    public void setCanastasTotal(Integer canastasTotal) {
        this.canastasTotal = canastasTotal;
    }

    public Integer getAsistenciasTotal() {
        return asistenciasTotal;
    }

    public void setAsistenciasTotal(Integer asistenciasTotal) {
        this.asistenciasTotal = asistenciasTotal;
    }

    public Integer getRebotesTotal() {
        return rebotesTotal;
    }

    public void setRebotesTotal(Integer rebotesTotal) {
        this.rebotesTotal = rebotesTotal;
    }

    public String getPosicion() {
        return posicion;
    }

    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }

    public Equipo getEquipo() {
        return equipo;
    }

    public void setEquipo(Equipo equipo) {
        this.equipo = equipo;
    }

    public Equipo getJugadorEquipo() {
        return jugadorEquipo;
    }

    public void setJugadorEquipo(Equipo equipo) {
        this.jugadorEquipo = equipo;
    }

    public Set<Estadisticas> getJugadorEstadisticass() {
        return jugadorEstadisticass;
    }

    public void setJugadorEstadisticass(Set<Estadisticas> estadisticass) {
        this.jugadorEstadisticass = estadisticass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Jugador jugador = (Jugador) o;
        return Objects.equals(id, jugador.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Jugador{" +
            "id=" + id +
            ", nombre='" + nombre + "'" +
            ", fechaNacimiento='" + fechaNacimiento + "'" +
            ", canastasTotal='" + canastasTotal + "'" +
            ", asistenciasTotal='" + asistenciasTotal + "'" +
            ", rebotesTotal='" + rebotesTotal + "'" +
            ", posicion='" + posicion + "'" +
            '}';
    }
}
